import axios from 'axios'
import Link from 'next/link'
import { useRouter } from 'next/router'
import { useEffect, useState } from 'react'
import { setGlobalState, useGlobalState } from '../../utils/globalState'

export default function ProductItem({ product }: any) {
  const [auth]: any = useGlobalState('auth')
  const [error, setError] = useState('')
  const router = useRouter()
  const [currentOrder, setCurrentOrder]: any = useState({})

  function addToCart(product: any) {
    if (Object.keys(auth).length === 0) {
      router.push('/signin')
      return
    }
    if (product.stock === 0) {
      setError('This product is out of stock.')
      return
    }
    axios
      .patch('/api/order/cart/nothing', {
        action: 'add',
        email: auth.user.email,
        product: product,
        productQuantity: 1,
      })
      .then((res) => {
        setCurrentOrder(res.data.currentOrder)
        setGlobalState('auth', { ...auth, cartQuantity: auth.cartQuantity + 1 })
      })
  }

  return (
    <Link href={`/product/${product._id}`}>
      <a className="mr-10 mb-10 max-w-sm rounded-lg border border-gray-200 bg-white shadow-md hover:bg-gray-300 dark:border-gray-700 dark:bg-gray-800">
        <img
          className="h-52 w-96 rounded-t-lg object-cover"
          src={product.images[0].url}
          alt={product.images[0].url}
        />
        <div className="mb-5 px-5 pt-5">
          <h5 className="h-16 text-xl font-bold tracking-tight text-gray-900 dark:text-white">
            {product.title}
          </h5>
          <h5 className="mb-2 font-bold tracking-tight text-gray-900 dark:text-white">
            ${product.price}
          </h5>
          <p className="h-28 font-normal text-gray-700 dark:text-gray-400">
            {product.description}
          </p>
          <Link href="">
            <button
              onClick={() => addToCart(product)}
              className="inline-flex w-full items-center justify-center rounded-lg bg-black py-2  text-sm font-medium text-white hover:bg-gray-700 "
            >
              Buy
            </button>
          </Link>
        </div>
      </a>
    </Link>
  )
}
