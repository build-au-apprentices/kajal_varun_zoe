export default function component({ user }: any) {
  return (
    <div>
      <p><b>Email:</b> {user.email}</p>
      <p><b>Joined:</b> {user.createdAt}</p>
    </div>
  )
}
