export default function component() {
    return (
        <div>
            <footer className=" pt-20 pb-20 bg-black text-white text-center lg:text-left ">
                <div className="text-center p-4">
                    © 2022 Dropshop
                </div>
            </footer>
        </div>)
}