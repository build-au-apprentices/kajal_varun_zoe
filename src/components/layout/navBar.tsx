import Link from 'next/link'
import { setGlobalState, useGlobalState } from '../../utils/globalState'
import Cookie from 'js-cookie'
import { useRouter } from 'next/router'
import SearchBar from './searchBar'
import { useEffect, useState } from 'react'
import axios from 'axios'

export default function component() {
  const [auth]: any = useGlobalState('auth')
  const router = useRouter()

  useEffect(() => {
    const loggedInStatus = localStorage.getItem('loggedIn') as any
    if (loggedInStatus === 'true') {
      axios.post('/api/user/getAccessToken').then((res) => {
        if (res.data.err) {
          return localStorage.removeItem('loggedIn')
        }
        setGlobalState('auth', {
          token: res.data.access_token,
          user: res.data.user,
          cartQuantity: res.data.cartQuantity,
        })
      })
    }
  }, [])

  function handleProfileList(e: any) {
    e.preventDefault()
    if (e.target.value === 'sign_out') {
      handleSignOut()
    } else if (e.target.value === 'profile') {
      router.push('/profile/' + auth.user.email)
    } else if (e.target.value === 'messages') {
      router.push('/profile/' + auth.user.email + '/messages')
    } else if (e.target.value === 'account') {
      router.push('/profile/' + auth.user.email + '/account')
    } else if (e.target.value === 'payments') {
      router.push('/profile/' + auth.user.email + '/payments')
    } else if (e.target.value === 'sales') {
      router.push('/profile/' + auth.user.email + '/sales')
    } else if (e.target.value === 'reviews') {
      router.push('/profile/' + auth.user.email + '/reviews')
    }
  }

  function handleSignOut() {
    Cookie.remove('refreshtoken', { path: '/api/user/access_token' })
    localStorage.removeItem('loggedIn')
    setGlobalState('auth', {})
    return router.push('/')
  }

  function signedInRouter() {
    return (
      <>
        <li className="nav-item mr-5">
          <Link href="/product/sell">
            <a className="block py-2 text-white no-underline hover:text-gray-400 md:border-none md:p-0">
              Sell
            </a>
          </Link>
        </li>
        <li className="nav-item mr-3">
          <Link href={'/order/cart'}>
            <a className="block flex py-2 text-white no-underline hover:text-gray-400 md:border-none md:p-0">
              <svg
                className="h-6 w-6"
                fill="none"
                stroke="currentColor"
                viewBox="0 0 24 24"
                xmlns="http://www.w3.org/2000/svg"
              >
                <path
                  strokeLinecap="round"
                  strokeLinejoin="round"
                  strokeWidth="2"
                  d="M3 3h2l.4 2M7 13h10l4-8H5.4M7 13L5.4 5M7 13l-2.293 2.293c-.63.63-.184 1.707.707 1.707H17m0 0a2 2 0 100 4 2 2 0 000-4zm-8 2a2 2 0 11-4 0 2 2 0 014 0z"
                ></path>
              </svg>
              <sup className="justtify-end font-bold">{auth.cartQuantity}</sup>
            </a>
          </Link>
        </li>
        <div className="nav-item flow-root">
          <div className="rounded hover:font-medium hover:text-gray-400 md:mx-2">
            <select
              className="custom-select text-capitalize bg-black"
              onChange={handleProfileList}
              value=""
            >
              <option hidden>{auth.user.email}</option>
              <option value="profile">Profile</option>
              <option value="messages">Messages</option>
              <option value="account">Account</option>
              <option value="payments">Payments</option>
              <option value="sales">Sales</option>
              <option value="reviews">Reviews</option>
              <option value="sign_out">Sign Out</option>
            </select>
          </div>
        </div>
      </>
    )
  }

  return (
    <header className="mb-20 min-w-min bg-black py-4 text-white shadow-lg md:flex md:items-center md:justify-between">
      <div className="mb-3 flex items-center justify-between ">
        <h1 className="mr-5 text-2xl text-white">
          <Link href="/">
            <a className="flex font-serif no-underline hover:text-gray-400">
              <svg
                className="mr-2 mt-1 h-6 w-6"
                fill="none"
                stroke="currentColor"
                viewBox="0 0 24 24"
                xmlns="http://www.w3.org/2000/svg"
              >
                <path
                  strokeLinecap="round"
                  strokeLinejoin="round"
                  strokeWidth="2"
                  d="M16 11V7a4 4 0 00-8 0v4M5 9h14l1 12H4L5 9z"
                ></path>
              </svg>
              <span className="justify-end">dropshop</span>
            </a>
          </Link>
        </h1>
        <a className="hover:text-orange text-white md:hidden">
          <i className="fa fa-2x fa-bars"></i>
        </a>
      </div>
      <SearchBar />
      <ul className="list-reset md:flex md:items-center">
        {auth.user ? (
          signedInRouter()
        ) : (
          <li className="nav-item">
            <Link href="/signin">
              <a className="py-2 text-white no-underline hover:text-gray-400">
                Signin
              </a>
            </Link>
          </li>
        )}
      </ul>
    </header>
  )
}
