import React from 'react';
import { render, screen } from "@testing-library/react";
import Footer from "../components/Footer.tsx";

describe('<Footer />', () => {
  beforeAll(() => {
    render(<Footer/>);
  });

  test('renders the footer', () => {
    expect(screen.getByTestId('footer')).toBeInTheDocument();
  });
});