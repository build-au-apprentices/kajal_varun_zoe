import NavBar from './navBar'
import Footer from './footer'

export default function component({ children }: any) {
    return (
        <div className="min-w-min">
            <NavBar />
            {children}
            <Footer />
        </div>
    )
}