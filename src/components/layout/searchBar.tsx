import React, { useState, useEffect } from 'react'
import filterSearch from '../../utils/filterSearch'
import { useRouter } from 'next/router'

export default function component() {
  const [search, setSearch] = useState('')
  const [sort, setSort] = useState('')
  const [category, setCategory] = useState('')
  const router = useRouter()

  function handleCategory(e: any) {
    setCategory(e.target.value)
  }

  function handleSort(e: any) {
    setSort(e.target.value)
  }

  function handleSubmit(e: any) {
    e.preventDefault()
    filterSearch({
      router,
      search: search ? search.toLowerCase() : 'all',
      sort: sort,
      category: category,
    })
  }

  return (
    <div className="mr-5 max-h-min w-full min-w-min pb-2 shadow-md md:flex">
      <div className="flex flex-col  bg-white pl-2 md:flex-row md:rounded-l-md">
        <div className="text-gray-800 hover:font-medium hover:text-gray-500 ">
          <select
            className="custom-select text-capitalize mt-1"
            value={category}
            onChange={handleCategory}
          >
            <option value="all">All</option>
            <option value="popular">Popular</option>
          </select>
        </div>
        <div className="text-gray-800 hover:font-medium hover:text-gray-500 ">
          <select
            className="custom-select text-capitalize mt-1"
            value={sort}
            onChange={handleSort}
          >
            <option value="price">Inc. Price</option>
            <option value="-price">Dec. Price</option>
          </select>
        </div>
      </div>
      <form onSubmit={handleSubmit} className="relative w-full ">
        <input
          value={search.toLowerCase()}
          onChange={(e) => setSearch(e.target.value)}
          type="text"
          className="focus:shadow-outline w-full min-w-max bg-white py-2  pl-3 text-sm leading-tight text-black focus:outline-none md:rounded-r-md"
          placeholder="search"
        />
        {search !== '' && (
          <button
            onClick={() => setSearch('')}
            className="absolute inset-y-0 right-10 w-10  max-w-min items-center justify-center  md:rounded"
            type="button"
          >
            <svg
              className="h-4 w-4 text-black"
              fill="none"
              stroke="currentColor"
              viewBox="0 0 24 24"
              xmlns="http://www.w3.org/2000/svg"
            >
              <path
                stroke-linecap="round"
                stroke-linejoin="round"
                stroke-width="2"
                d="M6 18L18 6M6 6l12 12"
              ></path>
            </svg>
          </button>
        )}

        <button
          className="absolute inset-y-0 right-0 w-10  max-w-min items-center justify-center bg-white pr-3 md:rounded"
          type="submit"
        >
          <svg
            className="h-5 w-5 text-black"
            fill="currentColor"
            xmlns="http://www.w3.org/2000/svg"
            viewBox="0 0 24 24"
          >
            <path d="M16.32 14.9l5.39 5.4a1 1 0 0 1-1.42 1.4l-5.38-5.38a8 8 0 1 1 1.41-1.41zM10 16a6 6 0 1 0 0-12 6 6 0 0 0 0 12z" />
          </svg>
        </button>
      </form>
    </div>
  )
}
