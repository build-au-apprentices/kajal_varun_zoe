import Head from 'next/head'
import Link from 'next/link'
import { useRouter } from 'next/router'
import { useEffect, useState } from 'react'
import { setGlobalState, useGlobalState } from '../utils/globalState'
import valid from '../utils/valid'
import axios from 'axios'
import Cookie from 'js-cookie'

export default function component() {
  const initialState = { email: '', password: '', cf_password: '' }
  const [userData, setUserData] = useState(initialState)
  const { email, password, cf_password } = userData
  const [showValidError, setShowValidError] = useState('')
  const [auth] = useGlobalState('auth')
  const router = useRouter()

  function handleChangeInput(e: any) {
    const { name, value } = e.target
    setUserData({ ...userData, [name]: value })
  }

  async function handleSubmit(e: any) {
    e.preventDefault()
    try {
      const res: any = await axios.post('/api/user/signup', userData)
      if (res.data.err) {
        setShowValidError(res.data.err)
        return
      }
      // success so Login
      const loginRes: any = await axios.post('/api/user/signin', {
        email: userData.email,
        password: userData.password,
      })
      if (loginRes.data.err) {
        setShowValidError(loginRes.data.err)
        return
      }
    //   console.log('res:' + JSON.stringify(loginRes.data))
      setGlobalState('auth', {
        token: loginRes.data.access_token,
        user: loginRes.data.user,
        cartQuantity: res.data.cartQuantity,
      })
      Cookie.set('refreshtoken', loginRes.data.refresh_token, {
        path: '/api/user/access_token',
        expires: 7,
      })
      localStorage.setItem('loggedIn', 'true')
    } catch (resError: any) {
      console.log(resError)
    }
  }

  //for auto login
  useEffect(() => {
    if (Object.keys(auth).length !== 0) {
      router.push('/')
    }
  }, [auth])

  return (
    <div className="bg-grey-lighter flex flex-col">
      <Head>
        <title>Sign up</title>
      </Head>
      <div className="container mx-auto flex max-w-sm flex-1 flex-col items-center justify-center px-2">
        <div className="w-full rounded bg-white px-6 py-8 text-black shadow-md">
          <h1 className="mb-8 text-center text-3xl">Sign up</h1>
          <input
            name="email"
            value={email}
            onChange={handleChangeInput}
            type="email"
            className="border-grey-light mb-4 block w-full rounded border p-3"
            placeholder="Email"
          />

          <input
            value={password}
            onChange={handleChangeInput}
            type="password"
            className="border-grey-light mb-4 block w-full rounded border p-3"
            name="password"
            placeholder="Password"
          />
          <input
            name="cf_password"
            value={cf_password}
            onChange={handleChangeInput}
            type="password"
            className="border-grey-light mb-4 block w-full rounded border p-3"
            placeholder="Confirm Password"
          />

          <button
            onClick={handleSubmit}
            type="button"
            className="hover:bg-green-dark my-1 w-full rounded bg-black py-3 text-center text-white focus:outline-none"
          >
            Create Account
          </button>

          <div className="mt-10 text-red-500">{showValidError} </div>
        </div>

        <div className="text-grey-dark mt-6">
          {'Already have an account? '}
          <Link href="/signin">
            <a className="border-blue text-blue border-b no-underline">
              Sign in
            </a>
          </Link>
        </div>
      </div>
    </div>
  )
}
