import axios from 'axios'
import Head from 'next/head'
import Link from 'next/link'
import { useRouter } from 'next/router'
import { useState, useContext, useEffect } from 'react'
import { setGlobalState, useGlobalState } from '../../utils/globalState'

export default function DetailProduct(props: any) {
  const [product]: any = useState(props.products)
  const [tab, setTab] = useState(0)
  const [error, setError] = useState('')
  const router = useRouter()
  const [auth]: any = useGlobalState('auth')
  const [currentOrder, setCurrentOrder]: any = useState({})
  const [reviews]: any = useState(props.reviews)
  const [orderQuantity, setOrderQuantity] = useState(1)

  function addToCart(productItem: any) {
    if (!auth.user) {
      router.push('/signin')
      return
    }
    if (orderQuantity < 1 || orderQuantity > product.stock) {
      setError('Quantity not available from current stock levels.')
      return
    }
    axios
      .patch('/api/order/cart/nothing', {
        action: 'add',
        email: auth.user.email,
        product: productItem,
        productQuantity: orderQuantity,
      })
      .then((res) => {
        setCurrentOrder(res.data.currentOrder)
        setGlobalState('auth', {
          ...auth,
          cartQuantity: auth.cartQuantity + orderQuantity,
        })
      })
  }

  function handleOrderQuantityMinusClick(e: any) {
    e.preventDefault()
    if (orderQuantity >= 2) {
      setOrderQuantity(orderQuantity - 1)
    }
  }

  function handleOrderQuantityPlusClick(e: any) {
    e.preventDefault()
    if (orderQuantity < product.stock) {
      setOrderQuantity(orderQuantity + 1)
    }
  }

  function handleOrderQuantityChange(e: any) {
    e.preventDefault()
    const qty = Number(e.target.value.trim())
    if (isNaN(qty)) {
      setError('Quantity can only be natural number.')
      return
    }
    setOrderQuantity(qty)
  }

  function leftArrow() {
    if (tab - 1 >= 0) {
      setTab(tab - 1)
      return
    }
    setTab(product.images.length - 1)
  }

  function rightArrow() {
    if (tab + 1 < product.images.length) {
      setTab(tab + 1)
      return
    }
    setTab(0)
  }

  return (
    <div className="">
      <Head>
        <title>Product Details</title>
      </Head>

      <div className=" items-start justify-center py-12 md:flex ">
        {product.images && (
          <div className="relative">
            <img
              className="w-full rounded-lg"
              src={product.images[tab].url}
              alt={product.images[0].url}
            />
            <button
              className="absolute bottom-20 left-0"
              type="button"
              onClick={() => leftArrow()}
            >
              <svg
                className="h-10 w-10"
                fill="none"
                stroke="currentColor"
                viewBox="0 0 24 24"
                xmlns="http://www.w3.org/2000/svg"
              >
                <path
                  strokeLinecap="round"
                  strokeLinejoin="round"
                  strokeWidth="2"
                  d="M15 19l-7-7 7-7"
                ></path>
              </svg>
            </button>
            <button
              className="absolute bottom-20 right-0"
              type="button"
              onClick={() => rightArrow()}
            >
              <svg
                className="h-10 w-10"
                fill="none"
                stroke="currentColor"
                viewBox="0 0 24 24"
                xmlns="http://www.w3.org/2000/svg"
              >
                <path
                  stroke-linecap="round"
                  stroke-linejoin="round"
                  stroke-width="2"
                  d="M9 5l7 7-7 7"
                ></path>
              </svg>
            </button>
            {/* <div className="mt-3 flex items-center justify-between space-x-4 md:space-x-0">
              {product.images.map((img: any, index: any) => (
                <a className="hover:bg-gray">
                  <img
                    key={index}
                    src={img.url}
                    alt={img.url}
                    className="m-1 h-32 w-32 rounded-lg"
                    onClick={() => setTab(index)}
                  />
                </a>
              ))}
            </div> */}
          </div>
        )}
        <div className="mt-6 md:ml-6 md:mt-0 md:w-1/2 lg:ml-8 xl:w-2/5">
          <div className="border-gray-200 pb-6">
            <h1 className="text-xl font-semibold leading-7 lg:text-2xl lg:leading-6">
              {product.title}
            </h1>
          </div>
          <a href="#reviews" className="hover:text-gray-400 ">
            View Reviews
          </a>
          <div>
            <p className="mb-5 mt-5">Description: {product.description}</p>
            <p className="">Price: ${product.price}</p>
            <p className="">Sold: {product.sold}</p>
            <p className="">In Stock: {product.stock}</p>
            <p className="">
              Seller:{' '}
              <Link href={'/profile/' + product.email}>
                <a className="hover:text-gray-400">{product.email}</a>
              </Link>
            </p>
          </div>
          <div className="mt-5 mb-3">
            <i
              onClick={handleOrderQuantityMinusClick}
              className="fa fa-minus font-thin"
              aria-hidden="true"
            ></i>
            <input
              onChange={handleOrderQuantityChange}
              value={orderQuantity}
              className="mx-2 w-10 rounded text-center text-black"
              type="text"
            ></input>
            <i
              onClick={handleOrderQuantityPlusClick}
              className="fa fa-plus font-thin"
              aria-hidden="true"
            ></i>
          </div>
          <button
            onClick={() => addToCart(product)}
            className="mt-5 flex w-full items-center justify-center rounded bg-white py-4 text-black hover:bg-gray-400"
          >
            Buy
          </button>
          <div className="mt-10 text-red-500">{error} </div>
        </div>
      </div>

      <div id="reviews">
        <h1 className="mb-5 mt-20 text-xl font-semibold leading-7 lg:text-2xl lg:leading-6">
          Reviews
        </h1>
        <Link href={'/product/add_review/' + product._id}>
          <a className="text-xl font-semibold hover:text-gray-400">
            Add a Review
          </a>
        </Link>
        <div className="products mt-5 w-full">
          {reviews.length === 0 ? (
            <h2>No Reviews</h2>
          ) : (
            reviews.map(
              (review: {
                _id: any
                email: any
                rating: any
                description: any
              }) => (
                <div
                  key={review._id}
                  className="mr-10 mb-10 max-w-sm rounded-lg border border-gray-200 bg-white p-5 shadow-md dark:border-gray-700 dark:bg-gray-800"
                >
                  <h5 className="mb-2 text-2xl font-bold tracking-tight text-gray-900 dark:text-white">
                    <Link href={'/profile/' + review.email}>
                      <a className="hover:text-gray-400">{review.email}</a>
                    </Link>
                  </h5>
                  <h5 className="mb-2 text-xl font-bold tracking-tight text-gray-900 dark:text-white">
                    {' '}
                    {review.rating} / 5 stars
                  </h5>
                  <p className="mb-3 h-32 min-h-full font-normal text-gray-700 dark:text-gray-400">
                    {review.description}
                  </p>
                </div>
              )
            )
          )}
        </div>
      </div>
    </div>
  )
}

// server side rendering
export async function getServerSideProps({ params: { id } }: any) {
  const res = await axios.post(process.env.BASE_URL + '/api/product/get' + id)
  // will be passed to the page component as props
  const res2 = await axios.post(process.env.BASE_URL + '/api/review/getManyByProductId' + id)

  // console.log(res.data.products)

  return {
    props: { products: res.data.products, reviews: res2.data.reviews },
  }
}
