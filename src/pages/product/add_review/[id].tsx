import Head from 'next/head'
import Link from 'next/link'
import { useRouter } from 'next/router'
import { useEffect, useState } from 'react'
import { setGlobalState, useGlobalState } from '../../../utils/globalState'
import axios from 'axios'
import Cookie from 'js-cookie'

export default function component(props: any) {
  const [auth]: any = useGlobalState('auth')
  const router = useRouter()
  const [product]: any = useState(props.products)
  let [reviewData, setUserData] = useState({
    rating: '',
    description: '',
    email: '',
    productId: product._id,
  })
  const { rating, description, email, productId } = reviewData
  const [error, setError] = useState('')

  useEffect(() => {
    // for refresh with state
    if (localStorage.getItem('loggedIn') !== 'true') {
      router.push('/signin')
      return
    }
    if (!auth.user) {
      router.push('/refresh')
      return
    }
  }, [])

  function handleChangeInput(e: any) {
    const { name, value } = e.target
    setUserData({ ...reviewData, [name]: value })
  }

  async function handleSubmit(e: any) {
    e.preventDefault()
    const numRating = Number(rating)
    if (isNaN(numRating)) {
      setError('Rating can only be a number.')
      return
    }
    if (numRating > 5 || numRating < 0) {
      setError('Allowed rating range is 0 to 5 inclusive.')
      return
    }

    try {
      reviewData = { ...reviewData, email: auth.user.email }
      // console.log(JSON.stringify(auth.user.email))

      const res: any = await axios.post(
        '/api/review/add' + product._id,
        reviewData
      )
      if (res.data.err) {
        setError(res.data.err)
        return
      }
      router.push('/product/' + product._id)
    } catch (resError: any) {
      console.log(resError)
    }
  }

  return (
    <div className="bg-grey-lighter flex flex-col">
      <Head>
        <title>Add Review</title>
      </Head>
      <div className="container mx-auto flex max-w-sm flex-1 flex-col items-center justify-center px-2">
        <div className="w-full rounded bg-white  px-20 py-8 text-black shadow-md">
          <h1 className="mb-8 text-center text-3xl">
            Add a Review for {product.title}
          </h1>
          <p>Rating (out of 5)</p>
          <input
            name="rating"
            value={rating}
            onChange={handleChangeInput}
            type="text"
            className="border-grey-light mb-4 block w-full rounded border p-3"
          />
          <p>Description</p>
          <input
            name="description"
            value={description}
            onChange={handleChangeInput}
            type="text"
            className="border-grey-light mb-4 w-full rounded border p-3"
          />
          <button
            onClick={handleSubmit}
            type="button"
            className="hover:bg-green-dark my-1 w-full rounded bg-black py-3 text-center text-white focus:outline-none"
          >
            Add
          </button>
          <div className="mt-10 text-red-500">{error}</div>
        </div>
      </div>
    </div>
  )
}

// server side rendering
export async function getServerSideProps({ params: { id } }: any) {
  const res = await axios.post(process.env.BASE_URL + '/api/product/get' + id)
  // will be passed to the page component as props
  return {
    props: { products: res.data.products },
  }
}
