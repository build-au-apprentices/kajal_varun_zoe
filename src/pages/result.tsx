import { setGlobalState, useGlobalState } from '../utils/globalState'
import Head from 'next/head'
import { useState, useContext, useEffect } from 'react'
import ProductItem from '../components/product/item'
import filterSearch from '../utils/filterSearch'
import { useRouter } from 'next/router'
import axios from 'axios'
import Image from 'next/image'

export default function Home(props: { products: any; result: number }) {
  const [products, setProducts]: any = useState(props.products)
  const [page, setPage] = useState(1)
  const router = useRouter()

  useEffect(() => {
    setProducts(props.products)
  }, [props.products])

  useEffect(() => {
    if (Object.keys(router.query).length === 0) setPage(1)
  }, [router.query])

  function handleCheck(id: any) {
    products.forEach((product: { _id: any; checked: any }) => {
      if (product._id === id) product.checked = !product.checked
    })
    setProducts([...products])
  }

  function handleLoadmore() {
    setPage(page + 1)
    filterSearch({ router, page: page + 1 })
  }

  return (
    <div className="">
      <Head>
        <title>Result</title>
      </Head>

      <div className="products w-full">
        {products.length === 0 ? (
          <h2>No Products</h2>
        ) : (
          products.map((product: { _id: any }) => (
            <ProductItem
              key={product._id}
              product={product}
              handleCheck={handleCheck}
            />
          ))
        )}
      </div>

      {props.result < page * 6 ? (
        ''
      ) : (
        <button
          className="btn btn-outline-info d-block mx-auto mb-4"
          onClick={handleLoadmore}
        >
          Load more
        </button>
      )}
    </div>
  )
}

export function getServerSideProps({ query }: any) {
  const page = query.page || 1
  const category = query.category || 'all'
  const sort = query.sort || ''
  const search = query.search || 'all'

  const props: any = axios
    .get(
      `${process.env.BASE_URL}/api/product?limit=${
        page * 6
      }&category=${category}&sort=${sort}&title=${search}`
    )
    .then((res) => {
      return {
        props: {
          products: res.data.products,
          result: res.data.result,
        },
      }
    })
  return props
}
