import Head from 'next/head'
import Link from 'next/link'
import { useRouter } from 'next/router'
import { useEffect, useState } from 'react'
import { setGlobalState, useGlobalState } from '../utils/globalState'
import axios from 'axios'
import Cookie from 'js-cookie'

export default function component() {
  const initialState = { email: '', password: '' }
  const [userData, setUserData] = useState(initialState)
  const { email, password } = userData
  const [showError, setShowError] = useState('')
  const [auth]: any = useGlobalState('auth')
  const router = useRouter()

  function handleChangeInput(e: any) {
    const { name, value } = e.target
    setUserData({ ...userData, [name]: value })
  }

  async function handleSubmit(e: any) {
    e.preventDefault()
    try {
      const res: any = await axios.post('/api/user/signin', userData)
      if (res.data.err) {
        setShowError(res.data.err)
        return
      }
      // console.log(res.data)
      setGlobalState('auth', {
        token: res.data.access_token,
        user: res.data.user,
        cartQuantity: res.data.cartQuantity,
      })

      Cookie.set('refreshtoken', res.data.refresh_token, {
        path: '/api/user/access_token',
        expires: 7,
      })

      localStorage.setItem('loggedIn', 'true')
    } catch (resError) {
      console.log(resError)
    }
  }

  useEffect(() => {
    if (Object.keys(auth).length !== 0) {
      router.push('/')
    }
  }, [auth])

  return (
    <div className="bg-grey-lighter flex flex-col">
      <Head>
        <title>Sign in</title>
      </Head>
      <div className="container mx-auto flex max-w-sm flex-1 flex-col items-center justify-center px-2">
        <div className="w-full rounded bg-white px-6 py-8 text-black shadow-md">
          <h1 className="mb-8 text-center text-3xl">Sign in</h1>
          <input
            value={email}
            onChange={handleChangeInput}
            type="text"
            className="border-grey-light mb-4 block w-full rounded border p-3"
            name="email"
            placeholder="Email"
          />

          <input
            value={password}
            onChange={handleChangeInput}
            type="password"
            className="border-grey-light mb-4 block w-full rounded border p-3"
            name="password"
            placeholder="Password"
          />

          <button
            onClick={handleSubmit}
            type="button"
            className="hover:bg-green-dark my-1 w-full rounded bg-black py-3 text-center text-white focus:outline-none"
          >
            Sign in
          </button>
          <div className="mt-10 text-red-500">{showError} </div>
        </div>

        <div className="text-grey-dark mt-6">
          {"Don't have an account? "}
          <Link href="/signup">
            <a className="border-blue text-blue border-b no-underline">
              Sign up
            </a>
          </Link>
        </div>
      </div>
    </div>
  )
}
