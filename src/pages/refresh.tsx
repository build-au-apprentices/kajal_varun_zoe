import { useEffect } from "react"
import { useRouter } from 'next/router'

export default function component() {
    const router = useRouter()

    useEffect(() => {
        router.back()
    }, [])

    return (
        <div className="bg-black text-white">
            <h1 className="text-lg">Loading...</h1>
        </div>)
}