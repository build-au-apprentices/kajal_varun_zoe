import Stripe from 'stripe'

const stripe = new Stripe(process.env.STRIPE_SECRET_KEY,{
    apiVersion: '2020-08-27',
  })

export default async function handler(req: any, res: any) {
  if (req.method === 'POST') {
    try {
      const session = await stripe.checkout.sessions.create({
        mode: 'payment',
        payment_method_types: ['card'],
        line_items: req?.body?.items ?? [],
        success_url: `${req.headers.origin}/order/payment/success?session_id={checkoutSessionId}`,
        cancel_url: `${req.headers.origin}/`,
      })
      res.status(200).json(session)
    } catch (err: any) {
      res.status(500).json({ message: err.message })
    }
  } else {
    res.setHeader('Allow', 'POST')
    res.status(405).json({ message: 'Method not allowed' })
  }
}
