import Stripe from 'stripe'
import connectDB from '../../../../utils/connectDB'

connectDB()

const stripe = new Stripe(process.env.STRIPE_SECRET_KEY, {
  apiVersion: '2020-08-27',
})

export default async function handler(req: any, res: any) {
  const id = req.query.id
  try {
    if (!id.startsWith('cs_')) {
      throw Error('Incorrect Checkout Session ID.')
    }
    const checkout_session = await stripe.checkout.sessions.retrieve(id)
    res.json(checkout_session)
  } catch (err: any) {
    res.json({ statusCode: 500, message: err.message })
  }
}
