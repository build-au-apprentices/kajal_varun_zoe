import connectDB from '../../../utils/connectDB'
import reviewModel from '../../../models/reviewModel'

connectDB()

export default  async function post(req: any, res: any) {
  try {
    const { productId, email, rating, description } = req.body
    if (rating.trim().length === 0 || description.trim().length === 0) {
      return res.json({ err: 'Rating and Description required.' })
    }
    const save = await new reviewModel({
      productId,
      email,
      rating,
      description,
    }).save()
    if (!save) {
      return res.json({ error: 'Rating needs to be a number.' })
    }
    return res.json({ msg: 'success' })
  } catch (err: any) {
    return res.status(500).json({ err: err.message })
  }
}
