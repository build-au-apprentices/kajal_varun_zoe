import connectDB from '../../../utils/connectDB'
import reviewModel from '../../../models/reviewModel'

connectDB()

export default async (req: any, res: any) => {
  if (req.method == 'GET') {
    await get(req, res)
  }
}

async function get(req: any, res: any) {
  try {
    const { id } = req.query
    const reviews = await reviewModel.find({ email: id })
    if (reviews === null) {
      return res.json({ err: 'Reviews do not exist.' })
    }
    return res.json({ reviews: reviews })
  } catch (err: any) {
    return res.json({ err: err.message })
  }
}
