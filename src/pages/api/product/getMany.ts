import connectDB from '../../../utils/connectDB'
import productModel from '../../../models/productModel'

connectDB()

class APIfeatures {
    query: any
    queryString: any
    constructor(query: any, queryString: any) {
        this.query = query;
        this.queryString = queryString;
    }
    filtering() {
        const queryObj = { ...this.queryString }

        // const excludeFields = ['page', 'sort', 'limit']
        // excludeFields.forEach(el => delete (queryObj[el]))

        if (queryObj.category !== 'all')
            this.query.find({ category: queryObj.category })
        if (queryObj.title !== 'all')
            this.query.find({ title: { $regex: queryObj.title } })

        this.query.find()
        return this;
    }

    sorting() {
        if (this.queryString.sort) {
            const sortBy = this.queryString.sort.split(',').join('')
            this.query = this.query.sort(sortBy)
        } else {
            this.query = this.query.sort('-createdAt')
        }

        return this;
    }

    paginating() {
        const page = this.queryString.page * 1 || 1
        const limit = this.queryString.limit * 1 || 6
        const skip = (page - 1) * limit;
        this.query = this.query.skip(skip).limit(limit)
        return this;
    }
}

export default async function getProducts(req: any, res: any) {
    try {
        const features: any = new APIfeatures(productModel.find(), req.query)
            .filtering().sorting().paginating()

        const products = await features.query

        res.json({
            status: 'success',
            result: products.length,
            products
        })
    } catch (err:any) {
        return res.status(500).json({ err: err.message })
    }
}
