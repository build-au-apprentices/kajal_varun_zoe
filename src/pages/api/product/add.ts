
import connectDB from '../../../utils/connectDB'
import productModel from '../../../models/productModel'

connectDB()



export default async function post(req: any, res: any) {
  try {
    let data = req.body
    const save = await new productModel(data).save()
    res.json({ msg: 'success' })
  } catch (err: any) {
    return res.status(500).json({ err: err.message })
  }
}
