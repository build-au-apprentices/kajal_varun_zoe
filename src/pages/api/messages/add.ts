import connectDB from '../../../utils/connectDB'
import messagesModel from '../../../models/messagesModel'

connectDB()


export default async function endpoint(req: any, res: any) {
    try {
        const { email1, email2, newMessage } = req.body
        const messages1 = await messagesModel.findOne({
          email1: email1,
          email2: email2,
        })
        const messages2 = await messagesModel.findOne({
          email1: email2,
          email2: email1,
        })
        if (messages1 === null && messages2 === null) {
          const createMessages = await new messagesModel({
            email1: email1,
            email2: email2,
          }).save()
          const updateNewMessages = await messagesModel.updateOne(
            {
              email1: email1,
              email2: email2,
            },
            {
              $push: {
                messages: newMessage,
              },
            }
          )
          return res.json({ success: true })
        }
        if (messages1 === null) {
          const updateExistingMessages = await messagesModel.updateOne(
            {
              email1: email2,
              email2: email1,
            },
            {
              $push: {
                messages: newMessage,
              },
            }
          )
          return res.json({ success: true })
        }
        const updateExistingMessages = await messagesModel.updateOne(
          {
            email1: email1,
            email2: email2,
          },
          {
            $push: {
              messages: newMessage,
            },
          }
        )
        return res.json({ success: true })
      } catch (err: any) {
        return res.json({ err: err.message })
      }
  }