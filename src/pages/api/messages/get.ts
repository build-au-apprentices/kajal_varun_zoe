import connectDB from '../../../utils/connectDB'
import messagesModel from '../../../models/messagesModel'

connectDB()


export default async function endpoint(req: any, res: any) {
    try {
      const { email1, email2 } = req.body
      const messages1 = await messagesModel.findOne({
        email1: email1,
        email2: email2,
      })
      const messages2 = await messagesModel.findOne({
        email1: email2,
        email2: email1,
      })
      if (messages1 === null && messages2 === null) {
        return res.json({ messages: [] })
      }
      if (messages1 === null) {
        return res.json({ messages: messages2.messages })
      }
      return res.json({ messages: messages1.messages })
    } catch (err: any) {
      return res.json({ err: err.message })
    }
  }