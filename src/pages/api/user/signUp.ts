import connectDB from '../../../utils/connectDB'
import Users from '../../../models/userModel'
import valid from '../../../utils/valid'
import bcryptjs from 'bcryptjs'
import orderModel from '../../../models/orderModel'

connectDB()

export default async function post(req: any, res: any) {
  try {
    const { email, password, cf_password } = req.body

    const errMsg = valid(email, password, cf_password)
    if (errMsg) return res.json({ err: errMsg })

    const user = await Users.findOne({ email })
    if (user) return res.json({ err: 'This email already exists.' })

    const passwordHash = bcryptjs.hashSync(password, 12)

    const saveUser = await new Users({
      email: email,
      password: passwordHash,
    }).save()
    const saveCart = await new orderModel({
      email: email,
      current: true,
    }).save()

    res.json({ msg: 'Register Success!' })
  } catch (err: any) {
    return res.status(500).json({ err: err.message })
  }
}
