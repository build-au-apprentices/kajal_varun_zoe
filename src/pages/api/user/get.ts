import connectDB from '../../../utils/connectDB'
import Users from '../../../models/userModel'

connectDB()

export default async function get(req: any, res: any) {
  try {
    const { id } = req.query
    const users = await Users.find({ email: id })
    if (users === null) {
      return res.json({ err: 'User does not exist.' })
    }
    return res.json({ users: users })
  } catch (err: any) {
    return res.json({ err: err.message })
  }
}
