import connectDB from '../../../utils/connectDB'
import userModel from '../../../models/userModel'
import orderModel from '../../../models/orderModel'
import jwt from 'jsonwebtoken'

interface JwtPayload {
  id: string
}

connectDB()

export default async function (req: any, res: any) {
  try {
    const rf_token = req.cookies.refreshtoken
    if (!rf_token) return res.status(400).json({ err: 'Please login now!' })

    const result = jwt.verify(
      rf_token,
      process.env.REFRESH_TOKEN_SECRET as any
    ) as JwtPayload
    if (!result)
      return res
        .status(400)
        .json({ err: 'Your token is incorrect or has expired.' })

    const user = await userModel.findById(result.id)
    if (user === null) {
      return res.json({ err: 'User do not exist.' })
    }
    // if (!user) return res.status(400).json({ err: 'User does not exist.' })

    const access_token = jwt.sign(
      { id: user._id },
      process.env.ACCESS_TOKEN_SECRET as any,
      { expiresIn: '15m' }
    )

    const currentOrder = await orderModel.findOne({
      email: user.email,
      current: true,
    })
    if (currentOrder === null) {
      return res.json({ err: 'Cart do not exist.' })
    }

    res.json({
      access_token,
      user: {
        email: user.email,
        role: user.role,
      },
      cartQuantity: currentOrder.quantity,
    })
  } catch (err: any) {
    return res.status(500).json({ err: err.message })
  }
}
