import connectDB from '../../../utils/connectDB'
import userModel from '../../../models/userModel'

connectDB()

export default async (req: any, res: any) => {
  if (req.method == 'PATCH') {
    await patch(req, res)
    return
  }
}

async function patch(req: any, res: any) {
  try {
    const { email, anothersEmail } = req.body
    const find = await userModel.findOne({
      email: email,
      messageEmails: [anothersEmail],
    })
    console.log(find)
    if (find === null) {
      const update1 = await userModel.updateOne(
        {
          email: email,
        },
        {
          $push: {
            messageEmails: anothersEmail,
          },
        }
      )
      const update2 = await userModel.updateOne(
        {
          email: anothersEmail,
        },
        {
          $push: {
            messageEmails: email,
          },
        }
      )
    }
    return res.json({ success: true })
  } catch (err: any) {
    return res.status(500).json({ err: err.message })
  }
}
