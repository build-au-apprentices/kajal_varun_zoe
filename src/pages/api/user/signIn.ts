import connectDB from '../../../utils/connectDB'
import userModel from '../../../models/userModel'
import orderModel from '../../../models/orderModel'
import bcryptjs from 'bcryptjs'
import jwt from 'jsonwebtoken'

connectDB()

export default async function signin(req: any, res: any) {
  try {
    const { email, password } = req.body

    const user = await userModel.findOne({ email })
    if (!user) return res.json({ err: 'This user does not exist.' })

    const isMatch = bcryptjs.compareSync(password, user.password)

    if (!isMatch) return res.json({ err: 'Incorrect password.' })

    const access_token = jwt.sign(
      { id: user._id },
      process.env.ACCESS_TOKEN_SECRET as any,
      { expiresIn: '15m' }
    )
    const refresh_token = jwt.sign(
      { id: user._id },
      process.env.REFRESH_TOKEN_SECRET as any,
      { expiresIn: '7d' }
    )

    const currentOrder = await orderModel.findOne({
      email: user.email,
      current: true,
    })

    console.log('test')

    res.json({
      msg: 'Login Success!',
      refresh_token,
      access_token,
      user: {
        email: user.email,
        role: user.role,
      },
      cartQuantity: currentOrder.quantity,
    })
  } catch (err: any) {
    return res.status(500).json({ err: err.message })
  }
}
