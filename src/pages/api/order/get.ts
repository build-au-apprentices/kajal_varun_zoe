import connectDB from '../../../utils/connectDB'
import orderModel from '../../../models/orderModel'

connectDB()

export default async function endpoint(req: any, res: any) {
  try {
    const { id } = req.query
    const order = await orderModel.findById(id)
    if (order === null) {
      return res.json({ err: 'Orders do not exist.' })
    }
    return res.json({ order: order })
  } catch (err: any) {
    return res.json({ err: err.message })
  }
}


