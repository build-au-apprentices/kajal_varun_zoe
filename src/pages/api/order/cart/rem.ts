import orderModel from '../../../../models/orderModel'
import connectDB from '../../../../utils/connectDB'

connectDB()

export default async function del(req: any, res: any) {
  try {
    const { email, checkoutSessionId } = req.body
    const updateCart = await orderModel.updateOne(
      { email: email, current: true },
      {
        $set: {
          current: false,
          checkoutSessionId: checkoutSessionId,
        },
      }
    )
    const saveCart = await new orderModel({
      email: email,
      current: true,
    }).save()

    res.json({ msg: true })
  } catch (err: any) {
    return res.status(500).json({ err: err.message })
  }
}
