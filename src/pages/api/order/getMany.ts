import connectDB from '../../../utils/connectDB'
import orderModel from '../../../models/orderModel'

connectDB()

export default async function get(req: any, res: any) {
  try {
    const { id } = req.query
    const orders = await orderModel.find({ email: id, current: false })
    if (orders === null) {
      return res.json({ err: 'Orders do not exist.' })
    }
    return res.json({ orders: orders })
  } catch (err: any) {
    return res.json({ err: err.message })
  }
}
