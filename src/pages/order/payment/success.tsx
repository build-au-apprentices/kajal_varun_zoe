import axios from 'axios'
import Head from 'next/head'
import { useRouter } from 'next/router'
import { useEffect, useState } from 'react'
import { setGlobalState, useGlobalState } from '../../../utils/globalState'

export default function component(props: any) {
  const [session]: any = useState(props.session)
  const [auth]: any = useGlobalState('auth')
  const router = useRouter()

  useEffect(() => {
    if (localStorage.getItem('loggedIn') !== 'true') {
      router.push('/signin')
      return
    }
    if (!auth.user) {
      router.push('/refresh')
      return
    }
    const axiosPost = async () => {
      const res = await axios.post('/api/order/cart/rem', {
        email: auth.user.email,
        checkoutSessionId: session.id,
      })
      // console.log(JSON.stringify(res.data))
      if (res.data.msg === true) {
        // console.log('cart updated from payment')
        setGlobalState('auth', {
          ...auth,
          cartQuantity: 0,
        })
      } else {
      }
    }
    axiosPost()
    return
  }, [])

  return (
    <div>
      <Head>
        <title>Payment Success</title>
      </Head>

      <div className="rounded-lg bg-white pl-2 text-black">
        <h1 className="text-2xl font-semibold">Payment Success!</h1>
        <p>Check your email for the receipt.</p>
        {/* {auth.user && <p>{JSON.stringify(session.payment_intent)}</p>} */}
      </div>
    </div>
  )
}

export async function getServerSideProps({ query }: any) {
  const res = await axios.post(
    process.env.BASE_URL + '/api/stripe/checkoutSession/' + query.session_id
  )
  return {
    props: { session: res.data },
  }
}
