import axios from 'axios'
import Head from 'next/head'
import { useRouter } from 'next/router'
import { useEffect, useState } from 'react'
import { useGlobalState } from '../../../../utils/globalState'

export default function component(props: any) {
  const [auth]: any = useGlobalState('auth')
  const router = useRouter()
  const [products, setProducts]: any = useState(props.products)

  useEffect(() => {
    if (localStorage.getItem('loggedIn') !== 'true') {
      router.push('/signin')
      return
    }
    if (!auth.user) {
      router.push('/refresh')
      return
    }
  }, [])

  return (
    <div>
      <Head>
        <title>Selling</title>
      </Head>
      <div className="pl-2 rounded-lg bg-white text-black ">
        <h1 className="text-2xl font-semibold">Your Products</h1>

        <div className="p-3">
          <div className="overflow-x-auto">
            <table className="w-full table-auto">
              <thead className="bg-gray-50 text-xs font-semibold uppercase text-gray-400">
                <tr>
                  <th className="whitespace-nowrap ">
                    <div className="text-left font-semibold">ID</div>
                  </th>
                  <th className="whitespace-nowrap ">
                    <div className="text-left font-semibold">Title</div>
                  </th>
                  <th className="whitespace-nowrap ">
                    <div className="text-left font-semibold">Description</div>
                  </th>
                  <th className="whitespace-nowrap ">
                    <div className="text-left font-semibold">Category</div>
                  </th>
                  <th className="whitespace-nowrap ">
                    <div className="text-left font-semibold">Price</div>
                  </th>
                  <th className="whitespace-nowrap ">
                    <div className="text-left font-semibold">Sold</div>
                  </th>
                  <th className="whitespace-nowrap ">
                    <div className="text-left font-semibold">Stock</div>
                  </th>
                </tr>
              </thead>
              <tbody className="divide-y divide-gray-100 text-sm">
                {products.map((product: any) => (
                  <tr key={product._id} className="my-4 text-black">
                    <td>{product._id}</td>
                    <td> {product.title}</td>
                    <td>{product.description}</td>
                    <td>{product.category}</td>
                    <td>${product.price}</td>
                    <td>{product.sold}</td>
                    <td> {product.stock}</td>
                  </tr>
                ))}
              </tbody>
            </table>
          </div>
        </div>
        
      </div>
    </div>
  )
}

export async function getServerSideProps({ params: { id } }: any) {
  const res = await axios.post(
    process.env.BASE_URL + '/api/product/getManyByEmail/' + id
  )
  return {
    props: { products: res.data.products },
  }
}
