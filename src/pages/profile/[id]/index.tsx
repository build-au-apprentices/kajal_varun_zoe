import axios from 'axios'
import Head from 'next/head'
import { useRouter } from 'next/router'
import { useEffect, useState } from 'react'
import { useGlobalState } from '../../../utils/globalState'
import ProductItem from '../../../components/product/item'
import Link from 'next/link'

export default function component(props: any) {
  const [auth]: any = useGlobalState('auth')
  const router = useRouter()
  const [products, setProducts]: any = useState(props.products)
  const [email, setEmail]: any = useState(props.email)

  useEffect(() => {
    if (!auth.user) {
      router.push('/refresh')
      return
    }
  }, [])

  async function handleSendMessage() {
    const res = await axios.post(
     '/api/user/setMessageEmails',
      { email: auth.user.email, anothersEmail: email }
    )
    if (res.data.success) {
      router.push(
        '/profile/' + auth.user.email + '/messages/' + email + '#latest'
      )
    }
  }

  return (
    <div>
      <Head>
        <title>Profile - {email}</title>
      </Head>
      <div className="">
        <h1 className="mb-10 text-2xl font-semibold">{email}'s Products</h1>
        {auth.user && (
          <a onClick={handleSendMessage} className="hover:text-gray-400">
            Message
          </a>
        )}

        <div className="products mt-10 w-full">
          {products.length === 0 ? (
            <h2>No Products</h2>
          ) : (
            products.map((product: { _id: any }) => (
              <ProductItem key={product._id} product={product} />
            ))
          )}
        </div>
      </div>
    </div>
  )
}

export async function getServerSideProps({ params: { id } }: any) {
  const res = await axios.post(
    process.env.BASE_URL + '/api/product/getByEmail/' + id
  )
  return {
    props: { email: id, products: res.data.products },
  }
}
