import axios from 'axios'
import Head from 'next/head'
import { useRouter } from 'next/router'
import { useEffect, useState } from 'react'
import { useGlobalState } from '../../../../utils/globalState'
import Link from 'next/link'

export default function component(props: any) {
  const [auth]: any = useGlobalState('auth')
  const router = useRouter()
  const [messages, setMessages]: any = useState(props.messages)
  const [email, setEmail]: any = useState(props.email)
  const [recipEmail, setRecipEmail]: any = useState(props.recipEmail)
  const [newMessage, setNewMessage]: any = useState([])

  useEffect(() => {
    if (localStorage.getItem('loggedIn') !== 'true') {
      router.push('/signin')
      return
    }
  }, [])

  function sendNewMessages() {
    const body = {
      email1: email,
      email2: recipEmail,
      newMessage: { [email]: newMessage },
    }
    axios.post('/api/message/get', body).then((res) => {
      if (res.data.success) {
        axios.post('/api/message/add', body).then((res) => {
          setMessages(res.data)
        })
        return
      }
      console.log('fail')
    })
  }

  return (
    <div>
      <Head>
        <title>Messages to {recipEmail}</title>
      </Head>
      <div className="rounded-md bg-white text-black ">
        <h1 className=" flex justify-center rounded-t-md border-2 border-gray-400 py-2 text-xl font-semibold">
          {recipEmail}
        </h1>

        {/* {JSON.stringify(messages.messages)} */}
        <div className="grid h-80 gap-3 overflow-auto text-white py-3">
          {messages.messages.map((message: any, index: number) => (
            <div key={index} className="">
              {messages.messages.length - 1 === index ? (
                Object.keys(message)[0] === email ? (
                  <div id="latest" className="flex justify-end">
                    <div className="mr-2 max-w-max rounded-t-3xl rounded-l-3xl bg-green-600 px-3 py-1">
                      {message[email]}
                    </div>
                  </div>
                ) : (
                  <div id="latest" className="flex justify-start">
                    <div className="ml-2 max-w-max rounded-t-3xl rounded-r-3xl  bg-blue-600 px-3 py-1">
                      {message[recipEmail]}
                    </div>
                  </div>
                )
              ) : Object.keys(message)[0] === email ? (
                <div className="flex justify-end">
                  <div className="mr-2 max-w-max rounded-t-3xl rounded-l-3xl bg-green-600 px-3 py-1">
                    {message[email]}
                  </div>
                </div>
              ) : (
                <div className="flex justify-start">
                  <div className="ml-2 max-w-max rounded-t-3xl rounded-r-3xl  bg-blue-600 px-3 py-1">
                    {message[recipEmail]}
                  </div>
                </div>
              )}
            </div>
          ))}
        </div>

        <form
          onSubmit={sendNewMessages}
          className="relative w-full rounded-b-md border-2 border-gray-400"
        >
          <input
            value={newMessage}
            onChange={(e) => setNewMessage(e.target.value)}
            type="text"
            className="focus:shadow-outline w-full min-w-max rounded-b-md bg-white  py-2 pl-2 text-sm leading-tight text-black focus:outline-none"
            placeholder="type message"
          />
          <button
            className="absolute inset-y-0 right-0 w-10  max-w-min items-center justify-center rounded-b-md bg-white pr-2"
            type="submit"
          >
            <svg
              className="h-6 w-6 text-green-600"
              fill="none"
              stroke="currentColor"
              viewBox="0 0 24 24"
              xmlns="http://www.w3.org/2000/svg"
            >
              <path
                strokeLinecap="round"
                strokeLinejoin="round"
                strokeWidth="2"
                d="M9 11l3-3m0 0l3 3m-3-3v8m0-13a9 9 0 110 18 9 9 0 010-18z"
              ></path>
            </svg>
          </button>
        </form>
      </div>
    </div>
  )
}

export function getServerSideProps({ params: { id, recipId } }: any) {
  const body = { email1: id, email2: recipId }
  const value = axios
    .post(process.env.BASE_URL + '/api/message', body)
    .then((res) => {
      return {
        props: {
          email: id,
          recipEmail: recipId,
          messages: res.data,
        },
      }
    })
  return value
}
