import axios from 'axios'
import Head from 'next/head'
import { useRouter } from 'next/router'
import { useEffect, useState } from 'react'
import { useGlobalState } from '../../../../utils/globalState'
import Link from 'next/link'

export default function component(props: any) {
  const [auth]: any = useGlobalState('auth')
  const router = useRouter()
  const [user, setUser]: any = useState(props.users[0])

  useEffect(() => {
    if (localStorage.getItem('loggedIn') !== 'true') {
      router.push('/signin')
      return
    }
    if (!auth.user) {
      router.push('/refresh')
      return
    }
  }, [])

  return (
    <div>
      <Head>
        <title>Messages</title>
      </Head>
      <div className="rounded-lg bg-white pb-2 pl-2 text-black">
        <h1 className="mb-5 pt-4 text-2xl font-semibold">Messages</h1>
        {user.messageEmails.map((message_email: any) => (
          <Link
            href={
              '/profile/' + user.email + '/messages/' + message_email + '#latest'
            }
          >
            <div className="mx-2 mb-2 rounded-md border-2 border-gray-600 hover:bg-gray-200">
              <a className="">{message_email}</a>
            </div>
          </Link>
        ))}

        {/* {JSON.stringify(user.messageEmails)} */}
      </div>
    </div>
  )
}

export async function getServerSideProps({ params: { id } }: any) {
  const res = await axios.post(process.env.BASE_URL + '/api/user/get' + id)
  return {
    props: { users: res.data.users },
  }
}
