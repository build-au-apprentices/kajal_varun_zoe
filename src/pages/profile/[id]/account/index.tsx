import axios from 'axios'
import Head from 'next/head'
import { useRouter } from 'next/router'
import { useEffect, useState } from 'react'
import { useGlobalState } from '../../../../utils/globalState'
import View from '../../../../components/profile/account/view'
import Link from 'next/link'

export default function component(props: any) {
  const [auth]: any = useGlobalState('auth')
  const router = useRouter()
  const [user, setUser]: any = useState(props.users[0])
  const [view, setView]: any = useState(true)

  useEffect(() => {
    if (localStorage.getItem('loggedIn') !== 'true') {
      router.push('/signin')
      return
    }
    if (!auth.user) {
      router.push('/refresh')
      return
    }
  }, [])

  return (
    <div>
      <Head>
        <title>Account</title>
      </Head>
      <div className="rounded-lg bg-white text-black pl-2">
        <h1 className=" text-2xl font-semibold">Account Details</h1>
        <br></br>
        <View user={user}></View>
        {/* <a onClick={() => setView(!view)} className="text-xl font-semibold">
          {view === true ? 'Edit' : 'View'}
        </a>
        :{view === true ? <View user={user}></View> : 'hi'} */}
      </div>
    </div>
  )
}

export async function getServerSideProps({ params: { id } }: any) {
  const res = await axios.post(process.env.BASE_URL + '/api/user/get' + id)
  return {
    props: { users: res.data.users },
  }
}
