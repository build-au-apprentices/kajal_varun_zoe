import axios from 'axios'
import Head from 'next/head'
import Link from 'next/link'
import { useRouter } from 'next/router'
import { useEffect, useState } from 'react'
import { useGlobalState } from '../../../../utils/globalState'

export default function component(props: any) {
  const [auth]: any = useGlobalState('auth')
  const router = useRouter()
  const [orders, setOrders]: any = useState(props.orders)

  useEffect(() => {
    if (localStorage.getItem('loggedIn') !== 'true') {
      router.push('/signin')
      return
    }
    if (!auth.user) {
      router.push('/refresh')
      return
    }
  }, [])

  async function refund(checkoutSessionId: any, index: number) {
    const res = await axios.post(
      '/api/stripe/refund/' + checkoutSessionId
    )
    if (res.data.refunded) {
      router.push('/refresh')
      return
    }
    // axios
    //   .get(process.env.BASE_URL + '/api/stripe/refund/' + checkoutSessionId)
    //   .then((res) => {
    //     if (res.data.refunded) {
    //       // router.push('/refresh')
    //       orders[index].refunded = true
    //       return
    //     }
    //   })
  }

  return (
    <div className="mx-auto flex h-full w-full flex-col justify-center  rounded-sm border border-gray-200 bg-white pl-2 text-black shadow-lg">
      <Head>
        <title>Order History</title>
      </Head>
      <div>
        <h1 className="text-2xl font-semibold">Order History</h1>
        <br></br>
        <div className="p-3">
          <div className="overflow-x-auto">
            <table className="w-full table-auto">
              <thead className="bg-gray-50 text-xs font-semibold uppercase text-gray-400">
                <tr>
                  <th className="whitespace-nowrap ">
                    <div className="text-left font-semibold">ID</div>
                  </th>
                  <th className="whitespace-nowrap ">
                    <div className="text-left font-semibold">Qty</div>
                  </th>
                  <th className="whitespace-nowrap ">
                    <div className="text-left font-semibold">Price</div>
                  </th>
                  <th className="whitespace-nowrap ">
                    <div className="text-left font-semibold">Date</div>
                  </th>
                  <th className="whitespace-nowrap ">
                    <div className="text-left font-semibold">Refund Status</div>
                  </th>
                </tr>
              </thead>
              <tbody className="divide-y divide-gray-100 text-sm">
                {orders.map((order: any, index: number) => (
                  <tr key={order._id} className="my-4 text-black">
                    <td>
                      {auth.user && (
                        <Link
                          href={
                            '/profile/' +
                            auth.user.email +
                            '/payments/order/' +
                            order._id
                          }
                        >
                          <a className="hover:underline">{order._id}</a>
                        </Link>
                      )}
                    </td>
                    <td>{order.quantity}</td>
                    <td>${order.price}</td>
                    <td>{order.updatedAt}</td>
                    <td>
                      {order.refunded ? (
                        'already refunded'
                      ) : (
                        <a
                          className="hover:underline"
                          onClick={() => {
                            refund(order.checkoutSessionId, index)
                          }}
                        >
                          refund
                        </a>
                      )}
                    </td>
                  </tr>
                ))}
              </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>
  )
}

export async function getServerSideProps({ params: { id } }: any) {
  const res = await axios.post(process.env.BASE_URL + '/api/order/getMany' + id)
  return {
    props: { orders: res.data.orders },
  }
}
