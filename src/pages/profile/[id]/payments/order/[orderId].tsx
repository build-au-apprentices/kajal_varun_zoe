import axios from 'axios'
import Head from 'next/head'
import { useRouter } from 'next/router'
import { useEffect, useState } from 'react'
import { useGlobalState } from '../../../../../utils/globalState'

export default function component(props: any) {
  const [auth]: any = useGlobalState('auth')
  const router = useRouter()
  const [order, setOrder]: any = useState(props.order)

  useEffect(() => {
    if (localStorage.getItem('loggedIn') !== 'true') {
      router.push('/signin')
      return
    }
    if (!auth.user) {
      router.push('/refresh')
      return
    }
  }, [])

  return (
    <div>
      <Head>
        <title>Past Order Details</title>
      </Head>
      <div className="rounded-lg bg-white text-black ">
        <h1 className="text-2xl font-semibold">Past Order Details</h1>
        <br></br>
        <p>Order ID: {order._id}</p>
        <p>Order Quantity: {order.quantity}</p>
        <p>Order Price: ${order.price}</p>
        <p>Order Date: {order.updatedAt}</p>

        <div className="p-3">
          <div className="overflow-x-auto">
            <table className="w-full table-auto">
              <thead className="bg-gray-50 text-xs font-semibold uppercase text-gray-400">
                <tr>
                  <th className="whitespace-nowrap ">
                    <div className="text-left font-semibold">Title</div>
                  </th>
                  <th className="whitespace-nowrap ">
                    <div className="text-left font-semibold">ID</div>
                  </th>
                  <th className="whitespace-nowrap ">
                    <div className="text-left font-semibold">Qty</div>
                  </th>
                  <th className="whitespace-nowrap ">
                    <div className="text-left font-semibold">Price</div>
                  </th>
                </tr>
              </thead>
              <tbody className="divide-y divide-gray-100 text-sm">
                {order.products.map((product: any) => (
                  <tr key={product.productId} className="my-4 text-black">
                    <td>{product.productTitle}</td>
                    <td>{product.productId}</td>
                    <td>{product.productQuantity}</td>
                    <td>${product.productPrice}</td>
                  </tr>
                ))}
              </tbody>
            </table>
          </div>
        </div>
        
      </div>
    </div>
  )
}

export async function getServerSideProps({ params: { orderId } }: any) {
  // console.log(JSON.stringify(orderId))
  const res = await axios.post(process.env.BASE_URL + '/api/order/get' + orderId)
  return {
    props: { order: res.data.order },
  }
}
