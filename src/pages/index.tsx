import { setGlobalState, useGlobalState } from '../utils/globalState'
import Head from 'next/head'
import { useState, useContext, useEffect } from 'react'
import filterSearch from '../utils/filterSearch'
import { useRouter } from 'next/router'
import axios from 'axios'
import Image from 'next/image'
import getStripe from '../utils/getStripe'

export default function Home() {
  const [page, setPage] = useState(1)
  const router = useRouter()

  useEffect(() => {
    if (Object.keys(router.query).length === 0) setPage(1)
  }, [router.query])

  function handleLoadmore() {
    setPage(page + 1)
    filterSearch({ router, page: page + 1 })
  }

  return (
    <div className="">
      <Head>
        <title>Home</title>
      </Head>

      <Image
        src="/homeBanner.png"
        alt="home_banner"
        width="100%"
        height="40%"
        layout="responsive"
      ></Image>
      <br />
      <br />
      <br />
      {/* <Image
        src="/sale_banner_2.jpg"
        alt="banner"
        width="90%"
        height="50%"
        layout="responsive"
      ></Image> */}
    </div>
  )
}
